<?php


namespace Sunandsand\Affiliate\Controller\Adminhtml\Affiliate;

use Magento\Framework\Exception\LocalizedException;

class Save extends \Magento\Backend\App\Action
{

    protected $dataPersistor;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
    ) {
        $this->dataPersistor = $dataPersistor;
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        if ($data) {
            $id = $this->getRequest()->getParam('affiliate_id');
        
            $model = $this->_objectManager->create('Sunandsand\Affiliate\Model\Affiliate')->load($id);
            if (!$model->getId() && $id) {
                $this->messageManager->addErrorMessage(__('This Affiliate no longer exists.'));
                return $resultRedirect->setPath('*/*/');
            }

            //echo "<pre>";
            //print_r($data);
            //function used to process image save
			//$data = $this->imageSave($data);
            $data = $this->imagePreprocessing($data);
            //print_r($data);
            //exit;
            $model->setData($data);
        
            try {
                $model->save();
                $this->messageManager->addSuccessMessage(__('You saved the Affiliate.'));
                $this->dataPersistor->clear('sunandsand_affiliate_affiliate');
        
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['affiliate_id' => $model->getId()]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the Affiliate.'));
            }
        
            $this->dataPersistor->set('sunandsand_affiliate_affiliate', $data);
            return $resultRedirect->setPath('*/*/edit', ['affiliate_id' => $this->getRequest()->getParam('affiliate_id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }
    /**
     * Image save method
     * $data form data
     * @return data 
     */
    public function imageSave($data){
        $this->adapterFactory = $this->_objectManager->create('Magento\Framework\Image\AdapterFactory');
        $this->uploader = $this->_objectManager->create('Magento\MediaStorage\Model\File\UploaderFactory');
        $this->filesystem = $this->_objectManager->create('Magento\Framework\Filesystem');

        unset($data['profile_image']);
        if(isset($data['affiliate_profile_image'][0])){
            $_FILES['affiliate_profile_image'] = $data['affiliate_profile_image'][0];
        }
        if (isset($_FILES['affiliate_profile_image']) && isset($_FILES['affiliate_profile_image']['name']) && strlen($_FILES['affiliate_profile_image']['name'])) 
        {
            try 
            {
                $base_media_path = 'boutiqaat_celebrity';
                $uploader = $this->uploader->create(['fileId' => 'affiliate_profile_image']);
                $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);
                $imageAdapter = $this->adapterFactory->create();
                $uploader->addValidateCallback('affiliate_profile_image', $imageAdapter, 'validateUploadFile');
                $uploader->setAllowRenameFiles(true);
                $uploader->setFilesDispersion(true);
                $mediaDirectory = $this->filesystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
                $result = $uploader->save($mediaDirectory->getAbsolutePath($base_media_path));
                $data['affiliate_profile_image'] = $base_media_path.$result['file'];
            }catch (\Exception $e) 
            {
                $this->messageManager->addError($e->getMessage() . "--" . $data['affiliate_profile_image']);
            }
        } 
        else 
        {
            if (isset($data['affiliate_profile_image']) && isset($data['affiliate_profile_image']['value'])) 
            {
                if (isset($data['affiliate_profile_image']['delete'])) 
                {
                    $data['affiliate_profile_image'] = null;
                    $data['delete_image'] = true;
                } 
                elseif (isset($data['affiliate_profile_image']['value'])) 
                {
                    $data['affiliate_profile_image'] = $data['affiliate_profile_image']['value'];
                } 
                else 
                {
                    $data['affiliate_profile_image'] = null;
                }
            }
        }
        
        return $data;
    }
    
        /**
     * Sets image attribute data to false if image was removed
     *
     * @param array $data
     * @return array
     */
    public function imagePreprocessing($data)
    {
        if (isset($data['affiliate_profile_image']) && isset($data['affiliate_profile_image'][0]['name']) && strlen($data['affiliate_profile_image'][0]['name'])) 
        {
            try 
            {
                $data['affiliate_profile_image'] = $data['affiliate_profile_image'][0]['name'];
            }catch (\Exception $e) 
            {
                $this->messageManager->addError($e->getMessage() . "--" . $data['affiliate_profile_image']);
            }
        } 
        return $data;
    }
}
