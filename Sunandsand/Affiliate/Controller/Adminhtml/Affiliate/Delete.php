<?php


namespace Sunandsand\Affiliate\Controller\Adminhtml\Affiliate;

class Delete extends \Sunandsand\Affiliate\Controller\Adminhtml\Affiliate
{

    /**
     * Delete action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        // check if we know what should be deleted
        $id = $this->getRequest()->getParam('affiliate_id');
        if ($id) {
            try {
                // init model and delete
                $model = $this->_objectManager->create('Sunandsand\Affiliate\Model\Affiliate');
                $model->load($id);
                $model->delete();
                // display success message
                $this->messageManager->addSuccessMessage(__('You deleted the Affiliate.'));
                // go to grid
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                // display error message
                $this->messageManager->addErrorMessage($e->getMessage());
                // go back to edit form
                return $resultRedirect->setPath('*/*/edit', ['affiliate_id' => $id]);
            }
        }
        // display error message
        $this->messageManager->addErrorMessage(__('We can\'t find a Affiliate to delete.'));
        // go to grid
        return $resultRedirect->setPath('*/*/');
    }
}
