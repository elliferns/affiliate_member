<?php


namespace Sunandsand\Affiliate\Controller\Adminhtml;

abstract class Affiliate extends \Magento\Backend\App\Action
{

    const ADMIN_RESOURCE = 'Sunandsand_Affiliate::top_level';
    protected $_coreRegistry;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry
    ) {
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context);
    }

    /**
     * Init page
     *
     * @param \Magento\Backend\Model\View\Result\Page $resultPage
     */
    public function initPage($resultPage)
    {
        $resultPage->setActiveMenu(self::ADMIN_RESOURCE)
            ->addBreadcrumb(__('Sunandsand'), __('Sunandsand'))
            ->addBreadcrumb(__('Affiliate'), __('Affiliate'));
        return $resultPage;
    }
}
