<?php


namespace Sunandsand\Affiliate\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\InstallSchemaInterface;

class InstallSchema implements InstallSchemaInterface
{

    /**
     * {@inheritdoc}
     */
    public function install(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $installer = $setup;
        $installer->startSetup();

        $table_sunandsand_affiliate = $setup->getConnection()->newTable($setup->getTable('sunandsand_affiliate'));

        
        $table_sunandsand_affiliate->addColumn(
            'affiliate_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            array('identity' => true,'nullable' => false,'primary' => true,'unsigned' => true,),
            'Entity ID'
        );
        

        
        $table_sunandsand_affiliate->addColumn(
            'name',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            null,
            [],
            'name'
        );
        

        
        $table_sunandsand_affiliate->addColumn(
            'affiliate_profile_image',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            null,
            [],
            'profile image'
        );
        

        
        $table_sunandsand_affiliate->addColumn(
            'status',
            \Magento\Framework\DB\Ddl\Table::TYPE_VARCHAR,
            null,
            [],
            'status'
        );
        

        
        $table_sunandsand_affiliate->addColumn(
            'created_at',
            \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
            null,
            ['DEFAULT' => 'CURRENT_TIMESTAMP'],
            'created_at'
        );
        

        
        $table_sunandsand_affiliate->addColumn(
            'updated_at',
            \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
            null,
            ['DEFAULT' => 'CURRENT_TIMESTAMP'],
            'updated_at'
        );
        

        $setup->getConnection()->createTable($table_sunandsand_affiliate);

        $setup->endSetup();
    }
}
