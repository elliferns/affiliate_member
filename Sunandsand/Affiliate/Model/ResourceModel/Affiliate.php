<?php


namespace Sunandsand\Affiliate\Model\ResourceModel;

class Affiliate extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('sunandsand_affiliate', 'affiliate_id');
    }
}
