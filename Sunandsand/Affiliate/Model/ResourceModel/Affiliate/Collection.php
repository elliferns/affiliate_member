<?php


namespace Sunandsand\Affiliate\Model\ResourceModel\Affiliate;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            'Sunandsand\Affiliate\Model\Affiliate',
            'Sunandsand\Affiliate\Model\ResourceModel\Affiliate'
        );
    }
}
