<?php


namespace Sunandsand\Affiliate\Model;

use Magento\Framework\Exception\CouldNotSaveException;
use Sunandsand\Affiliate\Model\ResourceModel\Affiliate as ResourceAffiliate;
use Sunandsand\Affiliate\Api\Data\AffiliateInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Reflection\DataObjectProcessor;
use Sunandsand\Affiliate\Api\Data\AffiliateSearchResultsInterfaceFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Sunandsand\Affiliate\Api\AffiliateRepositoryInterface;
use Magento\Framework\Api\SortOrder;
use Sunandsand\Affiliate\Model\ResourceModel\Affiliate\CollectionFactory as AffiliateCollectionFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Exception\CouldNotDeleteException;

class AffiliateRepository implements affiliateRepositoryInterface
{

    private $storeManager;

    protected $dataObjectProcessor;

    protected $affiliateFactory;

    protected $searchResultsFactory;

    protected $affiliateCollectionFactory;

    protected $resource;

    protected $dataObjectHelper;

    protected $dataAffiliateFactory;


    /**
     * @param ResourceAffiliate $resource
     * @param AffiliateFactory $affiliateFactory
     * @param AffiliateInterfaceFactory $dataAffiliateFactory
     * @param AffiliateCollectionFactory $affiliateCollectionFactory
     * @param AffiliateSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        ResourceAffiliate $resource,
        AffiliateFactory $affiliateFactory,
        AffiliateInterfaceFactory $dataAffiliateFactory,
        AffiliateCollectionFactory $affiliateCollectionFactory,
        AffiliateSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager
    ) {
        $this->resource = $resource;
        $this->affiliateFactory = $affiliateFactory;
        $this->affiliateCollectionFactory = $affiliateCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataAffiliateFactory = $dataAffiliateFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \Sunandsand\Affiliate\Api\Data\AffiliateInterface $affiliate
    ) {
        /* if (empty($affiliate->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $affiliate->setStoreId($storeId);
        } */
        try {
            $affiliate->getResource()->save($affiliate);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the affiliate: %1',
                $exception->getMessage()
            ));
        }
        return $affiliate;
    }

    /**
     * {@inheritdoc}
     */
    public function getById($affiliateId)
    {
        $affiliate = $this->affiliateFactory->create();
        $affiliate->getResource()->load($affiliate, $affiliateId);
        if (!$affiliate->getId()) {
            throw new NoSuchEntityException(__('affiliate with id "%1" does not exist.', $affiliateId));
        }
        return $affiliate;
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->affiliateCollectionFactory->create();
        foreach ($criteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                if ($filter->getField() === 'store_id') {
                    $collection->addStoreFilter($filter->getValue(), false);
                    continue;
                }
                $condition = $filter->getConditionType() ?: 'eq';
                $collection->addFieldToFilter($filter->getField(), [$condition => $filter->getValue()]);
            }
        }
        
        $sortOrders = $criteria->getSortOrders();
        if ($sortOrders) {
            /** @var SortOrder $sortOrder */
            foreach ($sortOrders as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        }
        $collection->setCurPage($criteria->getCurrentPage());
        $collection->setPageSize($criteria->getPageSize());
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        $searchResults->setTotalCount($collection->getSize());
        $searchResults->setItems($collection->getItems());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \Sunandsand\Affiliate\Api\Data\AffiliateInterface $affiliate
    ) {
        try {
            $affiliate->getResource()->delete($affiliate);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the affiliate: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($affiliateId)
    {
        return $this->delete($this->getById($affiliateId));
    }
}
