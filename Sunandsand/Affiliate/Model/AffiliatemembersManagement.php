<?php


namespace Sunandsand\Affiliate\Model;


class AffiliatemembersManagement
{

   
    /**
     * {@inheritdoc}
     */
    public function getAffiliatemembers($searchCriteria)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        
        $request = $objectManager->create('\Magento\Framework\App\RequestInterface');
        $affiliate = $objectManager->create('\Sunandsand\Affiliate\Model\Affiliate');
        $collection = $affiliate->getCollection();
        // print_r($collection->getData());
        //exit;
        
        // echo "<pre>";
        // $this->getRequest()->getParams(); 
        //echo $request->getParam('searchCriteria');
        if(count($request->getParams()) == 0){
            $response = $this->getAllAffiliates($collection);
            
        }else{
            $filterParams = $request->getParams();
            $response = $this->getFilteredAffiliates($collection, $filterParams['searchCriteria']['filterGroups'][0]['filters']);
        }
        
        return $response;
    }
    private function getFilteredAffiliates($collection, $filterOptions){
        $collection->addFieldToFilter($filterOptions[0]['field'],$filterOptions[0]['value']);
        return $collection->getData();
    }
    private function getAllAffiliates($collection){
        return $collection->getData();
    }
}
