<?php
namespace Sunandsand\Affiliate\Model\Config\Source;
use Magento\Framework\Option\ArrayInterface;
class Options implements ArrayInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value'=>'Enabled','label'=>'Enabled'],
            ['value'=>'Disabled','label'=>'Disabled']
        ];
    }
}