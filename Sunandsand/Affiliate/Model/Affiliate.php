<?php


namespace Sunandsand\Affiliate\Model;

use Sunandsand\Affiliate\Api\Data\AffiliateInterface;

class Affiliate extends \Magento\Framework\Model\AbstractModel implements AffiliateInterface
{

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Sunandsand\Affiliate\Model\ResourceModel\Affiliate');
    }

    /**
     * Get affiliate_id
     * @return string
     */
    public function getAffiliateId()
    {
        return $this->getData(self::AFFILIATE_ID);
    }

    /**
     * Set affiliate_id
     * @param string $affiliateId
     * @return \Sunandsand\Affiliate\Api\Data\AffiliateInterface
     */
    public function setAffiliateId($affiliateId)
    {
        return $this->setData(self::AFFILIATE_ID, $affiliateId);
    }

    /**
     * Get name
     * @return string
     */
    public function getName()
    {
        return $this->getData(self::NAME);
    }

    /**
     * Set name
     * @param string $name
     * @return \Sunandsand\Affiliate\Api\Data\AffiliateInterface
     */
    public function setName($name)
    {
        return $this->setData(self::NAME, $name);
    }

    /**
     * Get profile_image
     * @return string
     */
    public function getProfileImage()
    {
        return $this->getData(self::PROFILE_IMAGE);
    }

    /**
     * Set profile_image
     * @param string $profile_image
     * @return \Sunandsand\Affiliate\Api\Data\AffiliateInterface
     */
    public function setProfileImage($profile_image)
    {
        return $this->setData(self::PROFILE_IMAGE, $profile_image);
    }

    /**
     * Get status
     * @return string
     */
    public function getStatus()
    {
        return $this->getData(self::STATUS);
    }

    /**
     * Set status
     * @param string $status
     * @return \Sunandsand\Affiliate\Api\Data\AffiliateInterface
     */
    public function setStatus($status)
    {
        return $this->setData(self::STATUS, $status);
    }

    /**
     * Get created_at
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->getData(self::CREATED_AT);
    }

    /**
     * Set created_at
     * @param string $created_at
     * @return \Sunandsand\Affiliate\Api\Data\AffiliateInterface
     */
    public function setCreatedAt($created_at)
    {
        return $this->setData(self::CREATED_AT, $created_at);
    }

    /**
     * Get updated_at
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->getData(self::UPDATED_AT);
    }

    /**
     * Set updated_at
     * @param string $updated_at
     * @return \Sunandsand\Affiliate\Api\Data\AffiliateInterface
     */
    public function setUpdatedAt($updated_at)
    {
        return $this->setData(self::UPDATED_AT, $updated_at);
    }
}
