<?php


namespace Sunandsand\Affiliate\Api;

interface AffiliatemembersManagementInterface
{


    /**
     * GET for affiliatemembers api
     * @param string $param
     * @return string
     */
    
    public function getAffiliatemembers($param = '');
}
