<?php


namespace Sunandsand\Affiliate\Api\Data;

interface AffiliateSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{


    /**
     * Get affiliate list.
     * @return \Sunandsand\Affiliate\Api\Data\AffiliateInterface[]
     */
    
    public function getItems();

    /**
     * Set name list.
     * @param \Sunandsand\Affiliate\Api\Data\AffiliateInterface[] $items
     * @return $this
     */
    
    public function setItems(array $items);
}
