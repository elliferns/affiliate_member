<?php


namespace Sunandsand\Affiliate\Api\Data;

interface AffiliateInterface
{

    const AFFILIATE_ID = 'affiliate_id';
    const UPDATED_AT = 'updated_at';
    const PROFILE_IMAGE = 'profile_image';
    const NAME = 'name';
    const CREATED_AT = 'created_at';
    const STATUS = 'status';


    /**
     * Get affiliate_id
     * @return string|null
     */
    
    public function getAffiliateId();

    /**
     * Set affiliate_id
     * @param string $affiliate_id
     * @return \Sunandsand\Affiliate\Api\Data\AffiliateInterface
     */
    
    public function setAffiliateId($affiliateId);

    /**
     * Get name
     * @return string|null
     */
    
    public function getName();

    /**
     * Set name
     * @param string $name
     * @return \Sunandsand\Affiliate\Api\Data\AffiliateInterface
     */
    
    public function setName($name);

    /**
     * Get profile_image
     * @return string|null
     */
    
    public function getProfileImage();

    /**
     * Set profile_image
     * @param string $profile_image
     * @return \Sunandsand\Affiliate\Api\Data\AffiliateInterface
     */
    
    public function setProfileImage($profile_image);

    /**
     * Get status
     * @return string|null
     */
    
    public function getStatus();

    /**
     * Set status
     * @param string $status
     * @return \Sunandsand\Affiliate\Api\Data\AffiliateInterface
     */
    
    public function setStatus($status);

    /**
     * Get created_at
     * @return string|null
     */
    
    public function getCreatedAt();

    /**
     * Set created_at
     * @param string $created_at
     * @return \Sunandsand\Affiliate\Api\Data\AffiliateInterface
     */
    
    public function setCreatedAt($created_at);

    /**
     * Get updated_at
     * @return string|null
     */
    
    public function getUpdatedAt();

    /**
     * Set updated_at
     * @param string $updated_at
     * @return \Sunandsand\Affiliate\Api\Data\AffiliateInterface
     */
    
    public function setUpdatedAt($updated_at);
}
