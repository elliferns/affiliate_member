<?php


namespace Sunandsand\Affiliate\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface AffiliateRepositoryInterface
{


    /**
     * Save affiliate
     * @param \Sunandsand\Affiliate\Api\Data\AffiliateInterface $affiliate
     * @return \Sunandsand\Affiliate\Api\Data\AffiliateInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    
    public function save(
        \Sunandsand\Affiliate\Api\Data\AffiliateInterface $affiliate
    );

    /**
     * Retrieve affiliate
     * @param string $affiliateId
     * @return \Sunandsand\Affiliate\Api\Data\AffiliateInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    
    public function getById($affiliateId);

    /**
     * Retrieve affiliate matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Sunandsand\Affiliate\Api\Data\AffiliateSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete affiliate
     * @param \Sunandsand\Affiliate\Api\Data\AffiliateInterface $affiliate
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    
    public function delete(
        \Sunandsand\Affiliate\Api\Data\AffiliateInterface $affiliate
    );

    /**
     * Delete affiliate by ID
     * @param string $affiliateId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    
    public function deleteById($affiliateId);
}
